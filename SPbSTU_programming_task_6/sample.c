#include <stdio.h>
#include <stdlib.h>
#include "File.h"

#define STR_SIZE 200

int main(int argc, char **argv){
    system_init(100);
    char string[STR_SIZE] = {'\0'};
    File* input_file = open_file("testfile.txt", READ);
    File* output_file1 = open_file("test_output1", WRITE);
    File* output_file2 = open_file("test_output2", WRITE);

    while (read_from_file(input_file, string, STR_SIZE) != 0){
        write_to_file(output_file1, string, strlen(string));
        write_to_file(output_file2, string, strlen(string));
    }
    write_to_file(output_file1, string, strlen(string));
    write_to_file(output_file2, string, strlen(string));
    File* Adding_test = open_file("test_adding_file", WRITE_TO_END);
    write_to_file(Adding_test, "World!!!", 10);
    return 0;
}