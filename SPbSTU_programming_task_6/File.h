#ifndef _FILE_H
#define _FILE_H

#include <stdlib.h>
#include <unistd.h>
#include "fcntl.h"
#include <string.h>

typedef enum OpenFileType {READ, WRITE, WRITE_TO_END} FileState;

typedef struct {
    int descriptor;
    FileState state;
    void* cursor;
    void* buffer;
    char* name;
} File;

void system_init(int buff_size); // функция инициализации системы
void system_close(); // функция закрытия системы – здесь можно освободить память
File* open_file(char *name, FileState mode); //функиця открытия файла
void reopen_file(File* file, FileState new_mode);
char close_file(File* file); //функция закрытия файла
int read_from_file(File* file, void* output_str, int size_to_read); //функция чтения из файла
int write_to_file(File* file, void* input, int size_to_write); //функция записи в файл
#endif
