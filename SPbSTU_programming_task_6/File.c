#include "File.h"

#define min(X, Y) (((X) < (Y)) ? (X) : (Y))

static int buffer_size = 100;

static int amount_of_initialized_files = 0;
static int len_of_files = 20;
static File** files;

void system_init (int buff_size){
    buffer_size = buff_size + 1;
    files = (File**)malloc(sizeof(File*) * len_of_files);
};

void system_close(){
    File** cursor = files;
    File* remove_node = NULL;
    for (; len_of_files > 0; len_of_files--){
        if (*cursor != NULL){
            close(( *cursor )->descriptor);
            free((*cursor )->buffer);
            remove_node = *cursor;
            free(remove_node);
        }
        cursor++;
    }
    free(files);
};

File* open_file(char *name, FileState mode){
    int dc = -1;
    switch (mode){
    case READ:
        dc = open(name, O_RDONLY, 0);
        break;
    case WRITE:
        dc = creat(name, 0666);
        break;
    case WRITE_TO_END:
        dc = open(name, O_WRONLY, 0);
        if (dc != -1)
            lseek(dc, 0L, 2);
        break;
    }
    if (dc == -1)
        return NULL;

    File** file_place = files;
    if (amount_of_initialized_files >= len_of_files){
        len_of_files += 5;
        files = realloc(files, sizeof(File*) * len_of_files);
    }
    for (; (*file_place) != NULL; file_place++);
    (*file_place) = (File*)malloc(sizeof(File));
    (*file_place)->buffer = (char*)malloc(sizeof(char) * buffer_size);
    ((char*)(*file_place)->buffer)[buffer_size - 1] = 0;
    (*file_place)->cursor = ((char*)(*file_place)->buffer) + (buffer_size - 1);
    (*file_place)->state = mode;
    (*file_place)->descriptor = dc;
    (*file_place)->name = name;
    amount_of_initialized_files++;
    return (*file_place);
};

void reopen_file(File*file, FileState new_mode){
    int dc;
    close (file->descriptor);
    switch (new_mode){
    case READ:
        dc = open(file->name, O_RDONLY, 0);
        lseek(dc, file->cursor - file->buffer, 0);
        break;
    case WRITE:
        dc = creat(file->name, 0666);
        if (dc != -1)
            file->cursor = file->buffer;
        break;
    case WRITE_TO_END:
        dc = open(file->name, O_WRONLY, 0);
        if (dc != -1)
            lseek(dc, 0L, 2);
        break;
    }
    if (dc != -1){
        file->descriptor = dc;
    }
};

char close_file(File* file){
    int result = close(file->descriptor);
    if (result != -1){
        File** file_place = files;
        for (; strcmp((*file_place)->name, file->name) != 0; file_place++);
        free(file->buffer);
        free(file_place);
        *file_place = NULL;
    }
    amount_of_initialized_files--;
    return result;
};

int read_from_file(File* file, void* output_str, int size_to_read){
    int result = 0;
    int amount_write_to_src;
    memset (output_str, 0, size_to_read);
    size_to_read--;
    if (file->state == READ){
        int n_read;
        while (size_to_read != 0){
            amount_write_to_src = min(buffer_size - 1 - (file->cursor - file->buffer), size_to_read);
            size_to_read -= amount_write_to_src;
            memcpy(output_str, file->cursor, amount_write_to_src);
            output_str = (char*)output_str + amount_write_to_src;
            file->cursor = (char*)file->cursor + amount_write_to_src;
            if (size_to_read != 0){
                memset(file->buffer, '\0', buffer_size);
                n_read = read(file->descriptor, (char*)file->buffer, buffer_size - 1);
                if (n_read != buffer_size - 1){
                    int min_val = min(size_to_read, strlen(file->buffer));
                    strncat(output_str, file->buffer, min_val);
                    file->cursor = (char*)file->buffer + min(size_to_read, n_read);
                    return result;
                }
                file->cursor = file->buffer;
            }
            result += amount_write_to_src;
        }
        ((char*)output_str)[size_to_read] = '\0';
    }
    return result;
};

int write_to_file(File* file, void* input, int size_to_write){
    int result = 0;
    if (file->state != READ){
        result = write(file->descriptor, input, size_to_write);
    }
    return result;
};

