#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define FILE_NAME_LEN 100

enum parsing_state {IN_EMPTY_GROUP, IN_NUMBER_GROUP, IN_CHAR_GROUP};

int main(){
    
    char file_name[FILE_NAME_LEN];
    enum parsing_state state = IN_EMPTY_GROUP;
    
    long number_groups_counter, char_groups_counter;
    
    FILE *input_file;
      
    number_groups_counter = char_groups_counter = 0;
    

    printf("Enter name of input file\n");
    scanf("%s", file_name);
    
    
    input_file = fopen(file_name, "r");
    
    if (input_file == NULL){
        printf("Wrong file name\n");
        return 0;
    }
    
    char char_val;
    while((char_val = fgetc(input_file)) != EOF){
        if( isdigit(char_val)){
            if (state != IN_NUMBER_GROUP) {
                number_groups_counter ++;
                state = IN_NUMBER_GROUP;
            }
        }
        else if( isalpha(char_val)){
            if (state != IN_CHAR_GROUP){
                char_groups_counter ++;
                state = IN_CHAR_GROUP;
            }
        }
        else state = IN_EMPTY_GROUP;
    }
    fclose(input_file);


    printf("Values:\n\tnumber groups:  %ld\n\tchar groups: %ld\n", number_groups_counter, char_groups_counter);
    
    if (number_groups_counter > char_groups_counter)
        printf("Count of numbers groups more than chars groups");
    else
        printf("Count of numbers groups less or equal to chars groups");
    
    printf("\n");
    return 0;
}
