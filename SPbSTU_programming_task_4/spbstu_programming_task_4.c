/* spbstu_programming_task_4.c
 *
 * Copyright 2020 Arseniy Alexeev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>

#define CELL_SIZE_PX 100
#define PX_TO_CELL_POS(A) ((A) / CELL_SIZE_PX)
static char x_cell_amount = 0;
static char y_cell_amount = 0;

struct point{
    signed int x;
    signed int y;
} queen, finish_pos;

enum cell_color_triger {BLACK = 0, WHITE = 1} color_trigger;
    
static cairo_surface_t *surface = NULL;
    
int get_in_range_value(char* request_text, int begin, int end){
    int result;
    printf("%s", request_text);
    scanf("%d", &result);
    putchar('\n');
    while (result < begin || result > end){
        printf("Incorrect value. Please try again.\n");
        printf("%s", request_text);
        scanf("%d", &result);
        putchar('\n');
    }
    return result;
}  

static void draw_palete(cairo_t *cr){
    color_trigger = BLACK;
    cairo_move_to(cr, 0, 0);
    cairo_translate(cr, 0, 0);
    for (int x = 0; x < x_cell_amount; x++){
        if (y_cell_amount % 2 == 0)
            color_trigger = !color_trigger;
        for (int y = 0; y < y_cell_amount; y++){
            cairo_rectangle(cr, x * CELL_SIZE_PX, y *  CELL_SIZE_PX, CELL_SIZE_PX, CELL_SIZE_PX);
            if (color_trigger == BLACK)
                cairo_set_source_rgb (cr, 0, 0, 0);
            else
                cairo_set_source_rgb (cr, 1, 1, 1);
            cairo_fill (cr);
            cairo_stroke(cr);
            
            color_trigger = !color_trigger; 
        }
    }
}

static void draw_queen(cairo_t *cr, int cell_pos_x, int cell_pos_y){
    cairo_set_line_width(cr, 2);  
    cairo_set_source_rgb(cr, 1, 1, 1);

    cairo_arc(cr, (cell_pos_x + 0.5) * CELL_SIZE_PX, (cell_pos_y + 0.5) * CELL_SIZE_PX, CELL_SIZE_PX / 2 - 3, 0, 2 * M_PI);
    cairo_stroke_preserve(cr);
    cairo_set_source_rgb(cr, 0.8, 0.55, 0.09);
    cairo_fill(cr);    
    
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_move_to(cr, (cell_pos_x + 0.2) * CELL_SIZE_PX, (cell_pos_y + 0.75) * CELL_SIZE_PX);
    cairo_select_font_face(cr, "Purisa",
      CAIRO_FONT_SLANT_NORMAL,
      CAIRO_FONT_WEIGHT_BOLD);

    cairo_set_font_size(cr, CELL_SIZE_PX - 10);
    cairo_show_text(cr, "Q");
    cairo_translate(cr, 0, 0);
}


static void draw_win_strategy (cairo_t *cr){
        struct point position_diff;
        char steps_for_win;
        position_diff.x = finish_pos.x - queen.x;
        position_diff.y = finish_pos.y - queen.y;
        
        steps_for_win = (ABS(position_diff.x) == ABS(position_diff.y)) ? 1 : 2;
        if (ABS(position_diff.x) == 0 || ABS(position_diff.y) == 0)
            steps_for_win = 1;
        if (position_diff.x == 0 && position_diff.y == 0) steps_for_win = 0;
    
        if (steps_for_win == 2){
            cairo_set_line_width(cr, 2);
            cairo_set_source_rgba(cr, 0, 0, 0, 1);
            cairo_move_to(cr, (queen.x + position_diff.x + 0.5) * CELL_SIZE_PX, (queen.y + 0.5) * CELL_SIZE_PX);
            cairo_set_source_rgb(cr, 0.6, 0.5, 0.5);

            cairo_arc(cr, (queen.x + position_diff.x + 0.5) * CELL_SIZE_PX, (queen.y + 0.5) * CELL_SIZE_PX, CELL_SIZE_PX / 2 - 3, 0, 2 * M_PI);
            cairo_stroke_preserve(cr);
            cairo_set_source_rgb(cr, 0.6, 0.5, 0.4);
            cairo_fill(cr);    
            
            cairo_set_source_rgba(cr, 0,0,0,1);
            cairo_move_to(cr, (queen.x + position_diff.x + 0.2) * CELL_SIZE_PX, (queen.y + 0.75) * CELL_SIZE_PX);
            cairo_select_font_face(cr, "Purisa",
            CAIRO_FONT_SLANT_NORMAL,
            CAIRO_FONT_WEIGHT_BOLD);

            cairo_set_font_size(cr, CELL_SIZE_PX - 10);
            cairo_show_text(cr, "1");
            cairo_translate(cr, 0, 0);
        } 
        
        if (steps_for_win != 0){
            cairo_set_line_width(cr, 2);
            cairo_set_source_rgba(cr, 0, 0, 0, 1);
            cairo_move_to(cr, (finish_pos.x + 0.5) * CELL_SIZE_PX, (finish_pos.y + 0.5) * CELL_SIZE_PX);
            cairo_set_source_rgb(cr, 0, 0.7, 0);

            cairo_arc(cr, (finish_pos.x + 0.5) * CELL_SIZE_PX, (finish_pos.y + 0.5) * CELL_SIZE_PX, CELL_SIZE_PX / 2 - 3, 0, 2 * M_PI);
            cairo_stroke_preserve(cr);
            cairo_fill(cr);    
        }
}

static gboolean on_draw_event(GtkWidget* widget, cairo_t* cr, gpointer user_data){
    cairo_set_source_surface(cr, surface, 0, 0);
    cairo_paint(cr);
    GtkWidget *win = gtk_widget_get_toplevel(widget);
    draw_palete(cr);
    if (queen.x >= 0 && queen.y >= 0)
        draw_queen(cr, queen.x, queen.y);
    if (finish_pos.x >= 0 && finish_pos.y >= 0)
        draw_win_strategy(cr);
    gtk_widget_queue_draw(widget);
    return FALSE;
}

static gboolean mouse_klick_event_handler(GtkWidget* widget, GdkEventButton* event, gpointer data){
        if (event->button == 1){
            queen.x = PX_TO_CELL_POS((int)event->x);
            queen.y = PX_TO_CELL_POS((int)event->y);
        }
        else if (event->button == 3){
            finish_pos.x = PX_TO_CELL_POS((int)event->x);
            finish_pos.y = PX_TO_CELL_POS((int)event->y);
        }
        else{
            queen.x = -1;
            queen.y = -1;
            finish_pos.x = -1;
            finish_pos.y = -1;
        }
    return TRUE;
}

static gboolean configure_event_cb(GtkWidget *widget, GdkEventConfigure *event, gpointer data){
  if (surface)
      cairo_surface_destroy (surface);

  surface = gdk_window_create_similar_surface(gtk_widget_get_window (widget), CAIRO_CONTENT_COLOR, gtk_widget_get_allocated_width(widget), gtk_widget_get_allocated_height(widget));

  return TRUE;
}

static void activate (GtkApplication* app, gpointer user_data){
    GtkWidget *window;
    GtkWidget * drawing_area;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    drawing_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(window), drawing_area);

    
     g_signal_connect (drawing_area,"configure-event",
                    G_CALLBACK (configure_event_cb), NULL);
    
    g_signal_connect(G_OBJECT(drawing_area), "draw", G_CALLBACK(on_draw_event), NULL);
    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect (drawing_area, "button-press-event",
                    G_CALLBACK(mouse_klick_event_handler), NULL);
    gtk_widget_set_events (drawing_area, gtk_widget_get_events(drawing_area) | GDK_BUTTON_PRESS_MASK);

    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), x_cell_amount * CELL_SIZE_PX, y_cell_amount * CELL_SIZE_PX);
    gtk_window_set_resizable (GTK_WINDOW(window), FALSE);
    gtk_window_set_title(GTK_WINDOW(window), "Task answer");

    gtk_widget_show_all(window);

    gtk_main();
}

int main (int argc, char **argv){
    x_cell_amount = get_in_range_value("Please print amount of cell in weight: ", 1, 8);
    y_cell_amount = get_in_range_value("Please print amount of cell in height: ", 1, 8);
    queen.x = -1;
    queen.y = -1;
    finish_pos.x = -1;
    finish_pos.y = -1;
    
    GtkApplication *app;
    int status;

    app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
    status = g_application_run (G_APPLICATION (app), argc, argv);
    g_object_unref (app);
    return status;
}
