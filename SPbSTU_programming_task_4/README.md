# GTK_SPbSTU_task_4

The main point is request by console the checkerboard size and then write the solving
of queen walking path. If they can move from point to point by one step or by plural.
And the answer draws on the pallet.

How it works:

1) Set the size of the board in the console, then the board is drawn

![Start board](doc_src/start_pallete.png)

2) Click by the left mouse button of start queen position, ten queen pos is drawn.
3) Click by the right mouse button of the end queen position, then the end point will be drawn.

![Drawn direct answer](doc_src/direct_ans.png)

And if the queen needs more than one step, the program draws the first middle step

![Middle step solving](doc_src/middle_step_solve.png)

