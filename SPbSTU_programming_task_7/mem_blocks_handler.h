#ifndef MEM_BLOCK_HANDLER_H
#define MEM_BLOCK_HANDLER_H

#include <stdlib.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

typedef struct Mem{
    void* start;
    void* end;
    struct Mem* next;
    unsigned free;
} MemBlock;

typedef struct{
    MemBlock* first_mem_block;
    MemBlock* last_mem_block;
} MemoryHandler;

int add_new_mem_block(MemoryHandler* handler, void* mem_block_start, void* mem_block_end);
void* allocate_mem_block(MemoryHandler* handler, unsigned bytes);
void update_mem_handler(MemoryHandler* handler);
void free_mem_block(MemoryHandler* handler, void* mem_block_start);
int mem_block_size(MemoryHandler* handler, void* mem_block_start);
int mem_blocks_amount(MemoryHandler* handler);

#endif