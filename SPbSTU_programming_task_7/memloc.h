#ifndef MEMLOC_H
#define MEMLOC_H

#include <stdlib.h>
#include "mem_blocks_handler.h"

#ifndef MIN
#define MIN(A, B) (A < B) ? A : B
#endif

#define MEM_BUFFER_SIZE 256

void* mem_alloc(int bytes_amount);
void mem_free(void* memory_adr);
void* mem_realloc(void* memory_adr, int new_size);
MemoryHandler* get_mem_handler();

#endif
