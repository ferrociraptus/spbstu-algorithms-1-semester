#include "memloc.h"
#include <string.h>

#define TO_BUFFER_SIZES(A) ((A) % MEM_BUFFER_SIZE != 0) ? MEM_BUFFER_SIZE * (1 + (A) / MEM_BUFFER_SIZE): (A)

MemoryHandler* mem_handler;

void* mem_alloc(int bytes_amount){
    void* pointer;
    if (mem_handler == NULL){
        mem_handler = (MemoryHandler*)malloc(sizeof(MemoryHandler));
        mem_handler->first_mem_block = mem_handler->last_mem_block = NULL;
    }

    if ((pointer = allocate_mem_block(mem_handler, bytes_amount)) == NULL){
        int size = TO_BUFFER_SIZES(bytes_amount);
        void* buff = malloc(size);
        add_new_mem_block(mem_handler, buff, buff + (size - 1));
        pointer = allocate_mem_block(mem_handler, bytes_amount);
        pointer = buff;
    }
    return pointer;
}

void mem_free(void* memory_adr){
    if (mem_handler != NULL)
        free_mem_block(mem_handler, memory_adr);
}

void* mem_realloc(void* memory_adr, int new_size){
    if (mem_handler == NULL)
        return NULL;
    free_mem_block(mem_handler, memory_adr);
    void* new_adr = mem_alloc(new_size);
    if (new_adr == NULL)
        return new_adr;
    if (new_adr != memory_adr)
        memcpy(new_adr, memory_adr, MIN(mem_block_size(mem_handler, memory_adr), new_size));
    return new_adr;
}

MemoryHandler* get_mem_handler(){
    return mem_handler;
}