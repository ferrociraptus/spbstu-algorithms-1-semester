#include <stdio.h>
#include "memloc.h"

#define SIZE 1000

int main (){
    int* value =  (int*)mem_alloc(sizeof(int) * SIZE);
    char* value_char =  (char*)mem_alloc(sizeof(char) * 20);
    long* value_long =  (long*)mem_alloc(sizeof(long) * SIZE);
    for (int i = 0; i < SIZE; i++){
        value[i] = i;
        value_long[i] = i;
        if (i < 20)
            value_char[i] = '0' + i;
    }
    for (int i = 0; i < SIZE; i++)
        if (i < 20)
            printf("%d\n%ld\n%c\n", value[i], value_long[i], value_char[i]);
        else
            printf("%d\n%ld\n", value[i], value_long[i]);

    mem_free(value);
    mem_free(value_char);
    mem_free(value_long);
    return 0;

//     gchar* markup = "<span background=\"#00ff00\" foreground=\"#000000\"> Hello</span>";
//     gtk_label_set_markup(GTK_LABEL(name_label), markup);
}
