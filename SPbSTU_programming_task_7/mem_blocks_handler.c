#include "mem_blocks_handler.h"

int add_new_mem_block(MemoryHandler* handler, void* mem_block_start, void* mem_block_end){
    MemBlock* node;

    if (handler->first_mem_block == NULL){
        handler->first_mem_block = (MemBlock*)malloc(sizeof(MemBlock));
        node = handler->first_mem_block;
        handler->last_mem_block = handler->first_mem_block;
    }
    else{
        node = handler->first_mem_block;
        for(; node != handler->last_mem_block; node = node->next);
        node->next = (MemBlock*)malloc(sizeof(MemBlock));
        if (node->next == NULL)
            return -1;
        node = node->next;
    }
    node->start = mem_block_start;
    node->end = mem_block_end;
    node->next = NULL;
    node->free = TRUE;
    handler->last_mem_block = node;
//     update_mem_handler(handler);
    return 1;
}

void* allocate_mem_block(MemoryHandler* handler, unsigned bytes){

    int last_block_flag = FALSE;

    if (handler->first_mem_block == NULL)
        return NULL;
    MemBlock* node = handler->first_mem_block;

    while (!last_block_flag){
        if (node == handler->last_mem_block)
            last_block_flag = TRUE;
        if (node->end - node->start + 1 >= bytes && node->free == TRUE){
            if (node->end - node->start + 1 == bytes){
                node->free = FALSE;
                return node->start;
            }
            MemBlock* separated_mem_block = (MemBlock*)malloc(sizeof(MemBlock));
            separated_mem_block->start = node->start + bytes;
            separated_mem_block->end = node->end;
            separated_mem_block->free = TRUE;
            separated_mem_block->next = node->next;
            node->next = separated_mem_block;
            node->end = NULL;
            node->end = separated_mem_block->start - 1;
//             node->end = node->start + (bytes - 1);
            node->free = FALSE;
            if (node == handler->last_mem_block)
                handler->last_mem_block = node->next;
            return node->start;
        }
        node = node->next;
    }
    return NULL;
}

void update_mem_handler(MemoryHandler* handler){
    if (handler->first_mem_block != NULL){

        MemBlock* node = handler->first_mem_block;
        MemBlock* del_node;

        while (node->next != NULL){
            if ((node->next->start - node->end == 1 &&
                node->next->free == TRUE && node->free == TRUE) ||
                (node->next->end - node->next->start == 0)){
                    node->end = node->next->end;
                    if (node->next == handler->last_mem_block)
                        handler->last_mem_block = node;
                    del_node = node->next;
                    node->next = node->next->next;
                    free(del_node);
            }
            if (node->next != NULL)
                node = node->next;
        }
    }
}

void free_mem_block(MemoryHandler* handler, void* mem_block_start){
    if (handler->first_mem_block != NULL){

        MemBlock* node = handler->first_mem_block;
        int last_block_flag = FALSE;

        while (!last_block_flag){
            if (node->next == NULL)
                last_block_flag = TRUE;
            if (node->start == mem_block_start){
                node->free = TRUE;
                update_mem_handler(handler);
                return;
            }
            node = node->next;
        }
    }
}

int mem_block_size(MemoryHandler* handler, void* mem_block_start){
    if (handler->first_mem_block != NULL){

        MemBlock* node = handler->first_mem_block;
        int last_block_flag = FALSE;

        while (!last_block_flag){
            if (node->next == NULL)
                last_block_flag = TRUE;
            if (node->start == mem_block_start){
                return (node->end - node->start) + 1;
            }
            node = node->next;
        }

    }
    return -1;
}

int mem_blocks_amount(MemoryHandler* handler){
    if (handler->first_mem_block != NULL){
        MemBlock* node = handler->first_mem_block;
        unsigned blocks_amount = 0;

        while (node->next != NULL){
            blocks_amount += (node->end - node->start) + 1;
            if (node->next != NULL)
                node = node->next;
        }
        blocks_amount += (node->end - node->start) + 1;
        return blocks_amount;
    }
    return -1;
}


