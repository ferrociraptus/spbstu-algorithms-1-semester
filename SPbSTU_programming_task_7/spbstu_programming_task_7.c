#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "memloc.h"

#define WINDOW_SIZE_WEIGHT_PX 600
#define WINDOW_SIZE_HEIGHT_PX 400

#define MEM_INFO_NAME_LENGTH 150

enum {LIST_ITEM = 0, N_COLUMNS};

typedef struct{
    void* address;
    char name[100];
    unsigned int size;
} MemInfo;

GtkWidget* list;
GtkWidget* window;
static MemInfo* blocks;
static unsigned mem_info_amount;
static unsigned all_allocated_memory;


void* new_mem_info(){
    if (mem_info_amount == 0){
        blocks = (MemInfo*)mem_alloc(sizeof(MemInfo) * ++mem_info_amount);
        memset(blocks->name, '\0', MEM_INFO_NAME_LENGTH);
        blocks->address = NULL;
    }

    for (int i = 0; i < mem_info_amount; i++)
        if ((blocks + i)->address == NULL)
            return blocks + i;

    blocks = mem_realloc(blocks, ++mem_info_amount);
    memset((blocks + (mem_info_amount - 1))->name, '\0', MEM_INFO_NAME_LENGTH);
    (blocks + (mem_info_amount - 1))->address = NULL;
    return blocks + (mem_info_amount - 1);
}

void* find_mem_info(char* name){
    for (int i = 0; i < mem_info_amount; i++)
        if (strcmp((blocks + i)->name, name) == 0)
            return blocks + i;
    return NULL;
}

void append_item(GtkWidget* widget, gpointer* entry) {
    GtkListStore* store;
    GtkTreeIter iter;

    const gchar* name_str = gtk_entry_get_text(GTK_ENTRY(*entry));
    const gchar* size_str = gtk_entry_get_text(GTK_ENTRY(*(entry + 1)));

    const gint size = atoi(size_str);
    if (size == 0) {
        GtkWidget* dialog;
        GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
        dialog = gtk_message_dialog_new (GTK_WINDOW(window),
                                        flags,
                                        GTK_MESSAGE_ERROR,
                                        GTK_BUTTONS_CLOSE,
                                        "Wrong memory size value",
                                        NULL,
                                        g_strerror (errno));
        gtk_dialog_run (GTK_DIALOG (dialog));
        gtk_widget_destroy (dialog);
    }
    else if (strlen(name_str) != 0) {
        if (find_mem_info(name_str) == NULL){

            void* new_mem = mem_alloc(size);

            if (new_mem != NULL){
                MemInfo* new_info = new_mem_info();
                new_info->address = new_mem;
                memcpy(new_info->name, name_str, MIN(strlen(name_str), MEM_INFO_NAME_LENGTH));
                new_info->size = size;
                all_allocated_memory += size;

                store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(list)));
                gtk_list_store_append(store, &iter);
                gtk_list_store_set(store, &iter, LIST_ITEM, name_str, -1);
            }
        }
    }
    gtk_entry_set_text(*entry, "");
    gtk_entry_set_text(*(entry + 1), "");
}

void remove_item(GtkWidget* widget, gpointer selection) {
    GtkListStore* store;
    GtkTreeModel* model;
    GtkTreeIter  iter;
    gchar* value;

    store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(list)));
    model = gtk_tree_view_get_model(GTK_TREE_VIEW(list));

    if (gtk_tree_model_get_iter_first(model, &iter) == FALSE) {
        return;
    }

    if (gtk_tree_selection_get_selected(GTK_TREE_SELECTION(selection),
                                        &model, &iter)) {
        gtk_tree_model_get(model, &iter, LIST_ITEM, &value,  -1);
        MemInfo* current_info = find_mem_info(value);
        if (current_info != NULL){
            current_info->address = NULL;
            memset(current_info->name, '\0', MEM_INFO_NAME_LENGTH);
            all_allocated_memory -= current_info->size;
        }
        gtk_list_store_remove(store, &iter);
        g_free(value);
    }
}

void remove_all(GtkWidget* widget, gpointer selection) {

    GtkListStore* store;
    GtkTreeModel* model;
    GtkTreeIter  iter;

    store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(list)));
    model = gtk_tree_view_get_model(GTK_TREE_VIEW(list));

    if (gtk_tree_model_get_iter_first(model, &iter) == FALSE) {
        return;
    }

    gtk_list_store_clear(store);

    for (int i = 0; i < mem_info_amount; i++){
        blocks[i].address = NULL;
        memset(blocks[i].name, '\0', MEM_INFO_NAME_LENGTH);
    }
    all_allocated_memory = 0;
}

void on_changed(GtkWidget *widget, gpointer* labels) {

    GtkTreeIter iter;
    GtkTreeModel *model;
    gchar *value;

    if (gtk_tree_selection_get_selected(GTK_TREE_SELECTION(widget), &model, &iter)) {
        gtk_tree_model_get(model, &iter, LIST_ITEM, &value,  -1);

        MemInfo* info = find_mem_info(value);
        if (info == NULL)
            return;

        char formating_string[100];

        gtk_label_set_text(GTK_LABEL(*labels), info->name);
        sprintf(formating_string, "%p", info->address);
        gtk_label_set_text(GTK_LABEL(*(labels + 1)), formating_string);
        sprintf(formating_string, "%i", info->size);
        gtk_label_set_text(GTK_LABEL(*(labels + 2)), formating_string);
        sprintf(formating_string, "Variable memory usage: %u/%u bits", info->size, all_allocated_memory);
        gtk_progress_bar_set_text(GTK_PROGRESS_BAR(*(labels + 3)), formating_string);
        gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(*(labels + 3)), ((double)info->size)/all_allocated_memory);
        g_free(value);
    }
}

void init_list(GtkWidget* list) {

    GtkCellRenderer*    renderer;
    GtkTreeViewColumn*  column;
    GtkListStore*       store;

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes("List Item",
             renderer, "text", LIST_ITEM, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(list), column);

    store = gtk_list_store_new(N_COLUMNS, G_TYPE_STRING);

    gtk_tree_view_set_model(GTK_TREE_VIEW(list), GTK_TREE_MODEL(store));

    g_object_unref(store);
}

int main(int argc, char* argv[]) {
    GtkWidget* sw;

    GtkWidget* remove;
    GtkWidget* add;
    GtkWidget* remove_all;
    GtkWidget* entry_name;
    GtkWidget* entry_size;

    GtkWidget* vertical_box;
    GtkWidget* add_dell_box;
    GtkWidget* information_box;

    GtkWidget* name_label;
    GtkWidget* size_label;
    GtkWidget* info_name_label;
    GtkWidget* info_val_name_label;
    GtkWidget* info_address_label;
    GtkWidget* info_val_address_label;
    GtkWidget* info_size_label;
    GtkWidget* info_val_size_label;

    GtkWidget* progress_bar_of_memory_usage;

    GtkTreeSelection* selection;

    mem_info_amount = 0;

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    gtk_window_set_title(GTK_WINDOW(window), "List view");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);
    GdkRectangle workarea = {0};
    gdk_monitor_get_workarea(gdk_display_get_primary_monitor(gdk_display_get_default()),
    &workarea);
    GdkGeometry hints;
    hints.min_width = WINDOW_SIZE_WEIGHT_PX;
    hints.max_width = WINDOW_SIZE_WEIGHT_PX;
    hints.min_height = WINDOW_SIZE_HEIGHT_PX;
    hints.max_height = workarea.height;

    gtk_window_set_geometry_hints(GTK_WINDOW(window), window, &hints,(GdkWindowHints)(GDK_HINT_MIN_SIZE | GDK_HINT_MAX_SIZE));


    sw = gtk_scrolled_window_new(NULL, NULL);
    list = gtk_tree_view_new();
    gtk_container_add(GTK_CONTAINER(sw), list);

    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(sw),
                                        GTK_SHADOW_ETCHED_IN);

    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(list), FALSE);

    vertical_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);
    add_dell_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    information_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);

    add = gtk_button_new_with_label("Add");
    remove = gtk_button_new_with_label("Remove");
    remove_all = gtk_button_new_with_label("Remove All");
    entry_name = gtk_entry_new();
    entry_size = gtk_entry_new();
    gtk_widget_set_size_request(entry_name, 120, -1);
    gtk_widget_set_size_request(entry_size, 40, -1);

    GdkColor color;
    color.red = 25535;
    color.blue = 25535;
    color.green = 65535;

    name_label = gtk_label_new("Name:");
    size_label = gtk_label_new("Size in bits:");
    info_name_label = gtk_label_new("Name:");
    info_val_name_label = gtk_label_new("");
    info_address_label = gtk_label_new("Address:");
    info_val_address_label = gtk_label_new("");
    info_size_label = gtk_label_new("Size in bits:");
    info_val_size_label = gtk_label_new("");
    gtk_widget_set_size_request(info_val_name_label, 150, -1);
    gtk_widget_set_size_request(info_val_address_label, 150, -1);
    gtk_widget_set_size_request(info_val_size_label, 150, -1);

    progress_bar_of_memory_usage = gtk_progress_bar_new();
    gtk_progress_bar_set_show_text(GTK_PROGRESS_BAR(progress_bar_of_memory_usage), TRUE);
    gtk_progress_bar_set_text(GTK_PROGRESS_BAR(progress_bar_of_memory_usage), "Variable memory usage: 0/0 bits");

    gtk_box_pack_start(GTK_BOX(add_dell_box), add, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(add_dell_box), name_label, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(add_dell_box), entry_name, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(add_dell_box), size_label, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(add_dell_box), entry_size, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(add_dell_box), remove, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(add_dell_box), remove_all, FALSE, TRUE, 3);

    gtk_box_pack_start(GTK_BOX(information_box), info_name_label, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(information_box), info_val_name_label, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(information_box), info_address_label, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(information_box), info_val_address_label, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(information_box), info_size_label, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(information_box), info_val_size_label, FALSE, TRUE, 3);

    gtk_box_pack_start(GTK_BOX(vertical_box), sw, TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(vertical_box), add_dell_box, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(vertical_box), progress_bar_of_memory_usage, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(vertical_box), information_box, FALSE, TRUE, 3);

    gtk_container_add(GTK_CONTAINER(window), vertical_box);

    init_list(list);

    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(list));

    GtkWidget** add_data_container = (GtkWidget**)mem_alloc(sizeof(GtkWidget*) * 2);
    add_data_container[0] = entry_name;
    add_data_container[1] = entry_size;

    GtkWidget** mem_info_data_container = (GtkWidget**)mem_alloc(sizeof(GtkWidget*) * 4);
    mem_info_data_container[0] = info_val_name_label;
    mem_info_data_container[1] = info_val_address_label;
    mem_info_data_container[2] = info_val_size_label;
    mem_info_data_container[3] = progress_bar_of_memory_usage;

    g_signal_connect(G_OBJECT(add), "clicked",
                     G_CALLBACK(append_item), add_data_container);

    g_signal_connect(G_OBJECT(remove), "clicked",
                     G_CALLBACK(remove_item), selection);

    g_signal_connect(G_OBJECT(remove_all), "clicked",
                     G_CALLBACK(remove_all), selection);

    g_signal_connect(G_OBJECT(window), "destroy",
                     G_CALLBACK(gtk_main_quit), NULL);

    g_signal_connect(selection, "changed",
        G_CALLBACK(on_changed), mem_info_data_container);

    gtk_widget_show_all(window);

    gtk_main();
    mem_free(add_data_container);
    mem_free(mem_info_data_container);
    mem_free(blocks);
    return 0;
}
