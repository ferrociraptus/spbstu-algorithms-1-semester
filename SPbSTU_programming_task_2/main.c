#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define FILE_NAME_LEN 100

enum parsing_state {IN_WORD, IN_SPACE};

FILE* open_users_file(char string[],char parameters_to_open[]);
int pop_word_to_file(FILE* file, char str[], int word_index);
void push_string_to_file_with_equal_spaces(FILE* file, char str[], int words, unsigned int words_chars);

void fill_str_of_char(char ch, char str[], unsigned len);
void str_to_lower(char str[]);

int main()
{
    char ans[3];
    char use_hyphen_flag = 0;
    char is_new_line_flag = 0;
    int max_string_len = 0;
    unsigned int number_of_words = 0;
    unsigned int number_of_chars = 0;
    unsigned int number_of_non_spaces_chars = 0;
    
    enum parsing_state state = IN_SPACE;
    
    FILE *input_file, *output_file;
    
    input_file = open_users_file("Enter name of input file\n","r");
    output_file = open_users_file("Enter name of output file\n","wr");
    
    while(max_string_len < 2){
        printf("Enter max length of text line\n");
        scanf("%d", &max_string_len);
        if (max_string_len < 2){
            printf("Error: max length too small (min: 2). Please try again.\n");
        }
    }
    
    while (strcmp(ans, "yes") != 0 && strcmp(ans, "no") != 0){
        printf("Should I use a hyphen when wrap a word? (yes/no)\n");
        scanf("%3s", ans);
        str_to_lower(ans);
        if (strcmp(ans, "yes") == 0) use_hyphen_flag = 1;
        else if (strcmp(ans, "no") == 0) use_hyphen_flag = 0;
        else printf("Error: Wrong answer. Please try again.\n");
    }
    
    
    char *new_string = (char*) malloc(max_string_len * sizeof(char));
    fill_str_of_char(' ', new_string, max_string_len);
    
    char ch;
    while ((ch = fgetc(input_file)) != EOF){
        
        if (ch == '\n'){
            push_string_to_file_with_equal_spaces(output_file, new_string, number_of_words, number_of_non_spaces_chars);
            is_new_line_flag = 1;
        }
        else{
            if (!isprint(ch)) ch = ' ';
            
            if (ch == ' '){
                if (state != IN_SPACE){
                    number_of_chars++;
                    state = IN_SPACE;
                }
            }
            else{
                if (state != IN_WORD){
                state = IN_WORD;
                number_of_words++;
                }
                number_of_non_spaces_chars++;
                number_of_chars++;
            }
            
            
            if (number_of_chars <= max_string_len){
                if (number_of_chars > 0)
                new_string[number_of_chars-1] = ch;
            }
            else if(ch == ' '){
                push_string_to_file_with_equal_spaces(output_file, new_string, number_of_words, number_of_non_spaces_chars);
                is_new_line_flag = 1;
            }
            else if(number_of_words == 1){
                ungetc(ch, input_file);
                if (new_string[max_string_len - 2] != ' ' && use_hyphen_flag){
                    ungetc(new_string[max_string_len - 1], input_file);
                    new_string[max_string_len - 1] = '-';
                }
                push_string_to_file_with_equal_spaces(output_file, new_string, number_of_words, --number_of_non_spaces_chars);
                is_new_line_flag = 1;
            }
            else {
                ungetc(ch, input_file);
                number_of_non_spaces_chars -= pop_word_to_file(input_file, new_string, number_of_words);
                push_string_to_file_with_equal_spaces(output_file, new_string, --number_of_words, --number_of_non_spaces_chars);
                is_new_line_flag = 1;
            }
        }  
        if (is_new_line_flag){
            number_of_words = 0;
            number_of_non_spaces_chars = 0;
            number_of_chars = 0;
            state = IN_SPACE;
            is_new_line_flag = 0;
        }
    }
    push_string_to_file_with_equal_spaces(output_file, new_string, number_of_words, number_of_non_spaces_chars);
    
    fclose(input_file);
    fclose(output_file);
    printf("Program is end");
    return 0;
}

FILE* open_users_file(char string[],char parameters_to_open[]){
    FILE *file;
    char file_name[FILE_NAME_LEN];
    
    printf(string);
    scanf("%s", file_name);
    
    file = fopen(file_name, parameters_to_open);
    
    if (file == NULL){
        printf("Wrong file name\n");
        exit(0);
    }
    return file;
}

int pop_word_to_file(FILE* file, char str[], int word_index){
    // return len of word which was delited
    unsigned int len = strlen(str);
    unsigned int i = 0;
    unsigned int pop_word_len = 0;
    
    for (;word_index > 0; word_index--){
        while (str[i] == ' ') i++;

        while (str[i] != ' '&& i < len){
            i++;
        }
    }
    
    i--;
    while (i >= 0 && str[i] != ' '){
        ungetc(str[i], file);
        str[i] = ' ';
        pop_word_len++;
        i--;
    }
    
    return pop_word_len;
}

void push_string_to_file_with_equal_spaces(FILE* file, char str[], int words, unsigned int words_chars){
    unsigned int string_len = strlen(str);
    unsigned int *spaces = (unsigned int*) malloc((words + 1) * sizeof(unsigned int));
    int space = 0;
    int i = 0;
    unsigned int cursor = 0;
    if (words <= 0) return;
    
    if (words == 1){
            space = (string_len - words_chars - space*(words-1));
        if (space % 2 == 1){
            spaces[0] = space / 2 + 1;
            spaces[words] = spaces[0] - 1;
        }
        else spaces[0] = spaces[words] = space/2;
        
        while (i < words){
            for(int k = spaces[i]; k > 0; k--) fputc(' ', file);
            while (str[cursor] == ' ') cursor++;
            while (str[cursor] != ' ' && cursor < string_len){
                putc(str[cursor], file);
                cursor++;
            }
            i++;
        }
    }
    else{
        space = (string_len - words_chars)/(words-1);
        for (int i = 1; i < words; i++){
            spaces[i] = space;
        }
        
        space = (string_len - words_chars - space*(words-1));
        if (space % 2 == 1){
            spaces[0] = space / 2 + 1;
            spaces[words] = spaces[0] - 1;
        }
        else spaces[0] = spaces[words] = space/2;
        
        while (i < words){
            for(int k = spaces[i]; k > 0; k--) fputc(' ', file);
            while (str[cursor] == ' ') cursor++;
            while (str[cursor] != ' ' && cursor < string_len){
                putc(str[cursor], file);
                cursor++;
            }
            i++;
        }

    }
    for(int k = spaces[i]; k > 0; k--) fputc(' ', file);
    fputc('\n', file);
    
    for (i = 0; i < string_len ; i++) str[i] = ' ';
    return;
}

void str_to_lower(char str[]){
    for (int i = strlen(str) - 1; i >= 0; i--) str[i] = tolower(str[i]);
}

void fill_str_of_char(char ch, char str[], unsigned len){
    len--;
    for (;len > 0;len--) str[len] = ch;
}
