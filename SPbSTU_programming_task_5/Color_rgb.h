#ifndef COLOR_RGB_H
#define COLOR_RGB_H

typedef struct _ColorRGB{
    float red;
    float green;
    float blue;
} ColorRGB;

ColorRGB *copy_color(ColorRGB *color);
ColorRGB *new_color(float red, float green, float blue);
void destroy_color(ColorRGB* color);
void increase_color(ColorRGB* color, float red, float green, float blue);
void increase_all_color(ColorRGB* color, float val);
void add_to_color_color(ColorRGB* to, ColorRGB* from);
char is_white(ColorRGB *color);
char is_black(ColorRGB *color);

void make_white(ColorRGB *color);
void make_black(ColorRGB *color);

float in_color_range(float value);

#endif
