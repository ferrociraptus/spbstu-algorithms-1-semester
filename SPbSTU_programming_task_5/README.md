# Gtk tail visualization

The graphical application where the some kind of tail should follow the user comands.

Before complile you can configure application by selecting commands in [Config.h](./Config.h) file.
There you can select color of the tail and control interface between mouse following or the keyboard arrows use.

Sample of application work:

![The application screenshot](doc_src/work_sample.png)

