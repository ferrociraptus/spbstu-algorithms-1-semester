#include <stdio.h>

int main(void){
	unsigned long value;
	unsigned long result = 0;
	unsigned long number_bufer = 1;
	unsigned int min_power_of_two = 0;
	
	printf("Print natural number:\n");
	scanf("%ld", &value);
	
	if (value <= 0){
		printf("Number is not a natural number\n");
		return 0;
	}
	
	while (number_bufer <= value){
		number_bufer <<= 1;
		min_power_of_two++;
	}
	
	char right_bit;
	for (;min_power_of_two > 0;min_power_of_two--){
		result <<= 1;
		result |= value & 1;
		value >>= 1;
	}
	
	printf("Result number: %ld\n", result);

	return 0;
}